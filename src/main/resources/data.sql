insert into todo(id, title, description,created_date,completed_on,item_status)
values(10001,'Learn Java' ,'Java is a programming language and computing platform first released by Sun Microsystems in 1995', sysdate(), sysdate(),'TODO');

insert into todo(id, title,description,created_date,completed_on,item_status)
values(10002, 'Learn JPA','The Java Persistence API, in 2019 renamed to Jakarta Persistence', sysdate(), sysdate(),'TODO');

insert into todo(id, title,description,created_date,completed_on,item_status)
values(10003, 'Go to the GYM','A gymnasium, also known as a gym, is a covered location for gymnastics, athletics and gymnastic services', sysdate(), sysdate(),'DONE');