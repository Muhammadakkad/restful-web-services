package com.todo.rest.webservice.restfulwebservices.todo.model;

public enum ItemStatus {
  DONE, TODO, ON_GOING;
}
