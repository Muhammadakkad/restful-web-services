package com.todo.rest.webservice.restfulwebservices.todo.repo;

import com.todo.rest.webservice.restfulwebservices.todo.module.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToDoJpaRepository extends JpaRepository<Todo, Long> {
}
