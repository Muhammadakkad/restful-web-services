package com.todo.rest.webservice.restfulwebservices.todo.controller;

import com.todo.rest.webservice.restfulwebservices.todo.module.Todo;
import com.todo.rest.webservice.restfulwebservices.todo.repo.ToDoJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TodoJpaController {

    @Autowired
    private ToDoJpaRepository todoJpaRepository;


    @GetMapping("/jpa/todos")
    public List<Todo> getAllTodos() {
       return todoJpaRepository.findAll();
    }

    @GetMapping("/jpa/todos/{id}")
    public Todo getTodo(@PathVariable long id) {
        return todoJpaRepository.findById(id).get();
        }

    @DeleteMapping("/jpa/todos/{id}")
    public ResponseEntity<Void> deleteTodo(@PathVariable long id) {
        todoJpaRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/jpa/todos/{id}")
    public ResponseEntity<Todo> updateTodo(
            @PathVariable long id, @RequestBody Todo todo){

        Todo todoUpdated = todoJpaRepository.save(todo);

        return new ResponseEntity<Todo>(todo, HttpStatus.OK);
    }

    @PostMapping("/jpa/todos")
    public ResponseEntity<Void> createTodo(
            @RequestBody Todo todo){
        Todo createdTodo = todoJpaRepository.save(todo);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(createdTodo.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }
}