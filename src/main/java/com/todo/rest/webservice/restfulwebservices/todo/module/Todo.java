package com.todo.rest.webservice.restfulwebservices.todo.module;

import com.todo.rest.webservice.restfulwebservices.todo.model.ItemStatus;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Todo {

  @Id
  @GeneratedValue
  private Long id;

  private String title;

  private String description;

  private Date createdDate;

  private Date completedOn;

  @Enumerated(EnumType.STRING)
  @Column(length = 8, name="item_status")
  private ItemStatus itemStatus;

  protected Todo() {

  }

  public Todo(long id, String title, String description, Date createdDate, Date completedOn, ItemStatus itemStatus) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.createdDate = createdDate;
    this.completedOn = completedOn;
    this.itemStatus = itemStatus;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getcompletedOn() {
    return completedOn;
  }

  public void setcompletedOn(Date completedOn) {
    this.completedOn = completedOn;
  }

  public ItemStatus getItemStatus() {
    return itemStatus;
  }

  public void setItemStatus(ItemStatus itemStatus) {
    this.itemStatus = itemStatus;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Todo other = (Todo) obj;
    if (id != other.id)
      return false;
    return true;
  }

//    @Override
//    public int hashCode() {
//        return (int) (id ^ (id >>> 32));
//    }
}
